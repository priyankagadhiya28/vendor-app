<?php

Route::get('/', 'VendorController@index')->middleware(['auth.shop', 'billable'])->name('home');
Route::get('/webhooks/', 'WebHookController@get');
Route::get('/webhooks/delete', 'WebHookController@destroy');
Route::get('flush', function() {
    request()->session()->flush();
});

Route::get('/test', 'TestController@test');

//Route::get('/vue/{vue_capture?}', function () {
//    return view('layouts.app');
//})->where('vue_capture', '[\/\w\.-]*');


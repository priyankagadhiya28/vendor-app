<table>
    <tbody>
        <tr colspan='3'>
            <td> Order {{$data['name']}} </td>
        </tr>

        @foreach($data['line_items'] as $lineItem)
        <tr>
            <td>Product: {{$lineItem->title}} </td>
            <td> Variant: {{($lineItem->variant_title)? : '-' }} </td>
            <td> Sku: {{ ($lineItem->sku) ? : '-'}} </td>
            <td> Quantity: {{$lineItem->quantity}} </td>
        </tr>
        @endforeach
    </tbody>
</table>
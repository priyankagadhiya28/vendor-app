@extends('layouts.app')

@section('content')

<vendors-createdit props-urls='{!!
            json_encode([
                "index" => route("vendors.index"),
                "create" => route("vendors.create"),
                "store" => route("vendors.store"),          
                "edit" => route("vendors.edit",["__"]),
                "update" => route("vendors.update", ["__"]),                
                "destroy" => route("vendors.destroy",["__"]),
                "show" => route("vendors.show",["__"]),
            ],JSON_HEX_APOS)
        !!}'></vendors-createdit>

@endsection
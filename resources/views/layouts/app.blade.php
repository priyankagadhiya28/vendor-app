<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8" http-equiv="Content-Type" content="text/html;">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Vendor App</title>

        <!-- Fonts -->
        <!--        
            <link rel="dns-prefetch" href="//fonts.gstatic.com">
            <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        -->
        <link href="https://unpkg.com/ionicons@4.2.5/dist/css/ionicons.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">

        <!-- Styles -->

        <link href="https://sdks.shopifycdn.com/polaris/latest/polaris.css" rel="stylesheet" />
        
        <!--       
            <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
            <link href="{{ asset('css/style.css') }}" rel="stylesheet">        
        -->
    </head>

    <body>
        <div id="app">
<!--            <main class="Polaris-Frame__Main" id="AppFrameMain" data-has-global-ribbon="false">
                <div class="Polaris-Frame__Content">
                    <div class="Polaris-Page main-page">                   
                        <div class="Polaris-Page__Content" id="app">
                            <div class="Polaris-Card">                            
                                {{--@yield('content')--}}
                            </div>
                        </div>
                    </div>
                </div>
            </main>-->
        </div>

        <!-- Scripts -->                

        @if(config('shopify-app.esdk_enabled'))
        <script src="https://cdn.shopify.com/s/assets/external/app.js?{{ date('YmdH') }}"></script>
        <script type="text/javascript">
ShopifyApp.init({
    apiKey: '{{ config('shopify-app.api_key') }}',
    shopOrigin: 'https://{{ ShopifyApp::shop()->shopify_domain }}',
    debug: false,
    forceRedirect: true
});
        </script>

        @include('shopify-app::partials.flash_messages')
        @endif
        <script src="{{ asset(mix('js/app.js')) }}" ></script>
    </body>    
</html>

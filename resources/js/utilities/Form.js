import Errors from './Errors';

class Form {
    /**
     * Create a new Form instance.
     *
     * @param {object} data
     */
    constructor(data) {
        this.originalData = data;

        for (let field in data) {
            this[field] = data[field];
        }

        this.errors = new Errors();
    }

    /**
     * Fetch all relevant data for the form.
     */
    data() {
        let data = {};

        for (let property in this.originalData) {
            data[property] = this[property];
        }

        return this.createFormDataObj(new FormData(), data);
    }

    createFormDataObj(form_data, values, name) {
        if (!values && name) {
            form_data.append(name, '');
        } else {
            if (typeof values == 'object') {
                if (values instanceof File) {
                    form_data.append(name, values);
                } else {
                    for (let key in values) {
                        let new_key = (name == undefined) ? key : name + '[' + key + ']';                        
                        if (typeof values[key] == 'object')
                            this.createFormDataObj(form_data, values[key], new_key);
                        else{                            
                            form_data.append(new_key, values[key]);
                        }
                    }
                }
            } else{
                form_data.append(name, values);
            }
        }

        return form_data;
    }

    /**
     * Determine if an originalData exists for the given field.
     *
     * @param {string} field
     */

    has(field) {
        if (field.indexOf('.') != -1) {
            var result = true;
            var pathArr = field.split('.');
            var searchObject = this.originalData;
            $.each(pathArr, function (index, value) {
                if (index) {
                    var prev = index - 1;
                    result = Object.prototype.hasOwnProperty.call(searchObject[pathArr[prev]], pathArr[index]);

                    if (!result) {
                        return result;
                    } else {
                        searchObject = searchObject[pathArr[prev]];
                    }
                }
            });
            return result;
        } else {
            return this.originalData.hasOwnProperty(field);
        }
    }
    /**
     * Reset the form fields.
     */
    reset() {
        for (let field in this.originalData) {
            this[field] = '';
        }

        this.errors.clear();
    }

    /**
     * Send a GET request to the given URL.
     * .
     * @param {string} url
     */
    get(url) {
        return this.submit('get', url);
    }

    /**
     * Send a POST request to the given URL.
     * .
     * @param {string} url
     */
    post(url) {
        return this.submit('post', url);
    }

    /**
     * Send a PUT request to the given URL.
     * .
     * @param {string} url
     */
    put(url) {
        return this.submit('put', url);
    }

    /**
     * Send a PATCH request to the given URL.
     * .
     * @param {string} url
     */
    patch(url, resetData) {
        return this.submit('patch', url, resetData);
    }

    /**
     * Send a DELETE request to the given URL.
     * .
     * @param {string} url
     */
    delete(url) {
        return this.submit('delete', url);
    }

    /**
     * Submit the form.
     *
     * @param {string} requestType
     * @param {string} url
     */
    submit(requestType, url) {
        return new Promise((resolve, reject) => {
            axios[requestType](url, this.data())
                    .then(response => {
                        if (requestType != 'get') {
                            this.onSuccess(response.data);
                        }

                        resolve(response.data);
                    })
                    .catch(error => {
                        console.log(error.response.data);
                        this.onFail(error.response.data);

                        reject(error.response.data);
                    });
        });
    }

    /**
     * Handle a successful form submission.
     *
     * @param {object} data
     */
    onSuccess(data) {
//        this.reset();
    }

    /**
     * Handle a failed form submission.
     *
     * @param {object} errors
     */
    onFail(errors) {
        this.errors.record(errors);
    }
}
export default Form;
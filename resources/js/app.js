require('./bootstrap');

window.Vue = require('vue');
window.VueRouter = require('vue-router').default;
window.VueAxios = require('vue-axios').default;

//define component
let AppLayout = require('./components/layouts/App.vue').default;
const Setting = Vue.component('Setting', require('./components/layouts/Setting.vue').default);
const ListVendors = Vue.component('List', require('./components/layouts/vendors/List.vue').default);
const CreateEditVendor = Vue.component('CreateEdit', require('./components/layouts/vendors/CreateEdit.vue').default);
const ViewVendor = Vue.component('View', require('./components/layouts/vendors/View.vue').default);

const routes = [
    {
        name: 'ListVendors',
        path: '/',
        component: ListVendors
    },
    {
        name: 'CreateVendor',
        path: '/vendors/create',
        component: CreateEditVendor
    },
    {
        name: 'EditVendor',
        path: '/vendors/:id/edit',
        component: CreateEditVendor
    },
    {
        name: 'ViewVendor',
        path: '/vendors/:id',
        component: ViewVendor
    },
    {
        name: 'Setting',
        path: '/setting',
        component: Setting
    },
];

//Define vue routes
const router = new VueRouter({
    mode: 'history',
    routes: routes
})

//registering modules
Vue.use(VueRouter, VueAxios);




new Vue({
   el: '#app',
   router: router,
    render: h => h(AppLayout),
});

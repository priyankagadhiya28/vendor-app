<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->uuid('id')->unique()->primary();
            $table->integer('shop_id')->unsigned();
            $table->uuid('mail_service_id');
            $table->json('configuration');
            $table->timestamps();

            $table->foreign('shop_id')->references('id')->on('shops');
            $table->foreign('mail_service_id')->references('id')->on('mail_services');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->dropForeign('settings_mail_service_id__foreign');
            $table->dropForeign('settings_shop_id__foreign');
        });
        Schema::dropIfExists('settings');
    }
}

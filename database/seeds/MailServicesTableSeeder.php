<?php

use Illuminate\Database\Seeder;
use App\Models\MailService;

class MailServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $services = [];
        $service = [];
        $service['type'] = 'gmail';
        $service['driver'] = null;
        $service['fields'] = ['email', 'from'];
        $services[] = $service;

        $service = [];
        $service['type'] = 'smtp';
        $service['driver'] = 'smtp';
        $service['fields'] = ['host', 'port', 'username', 'password', 'from'];
        $services[] = $service;

        $service = [];
        $service['type'] = 'sparkpost';
        $service['driver'] = 'sparkpost';
        $service['fields'] = ['host', 'port', 'username', 'password', 'from'];
        $services[] = $service;

        foreach ($services as $service) {
            $mailService = new MailService;
            $mailService->id = uuid();
            $mailService->type = $service['type'];
            $mailService->driver = $service['driver'];
            $mailService->fields = $service['fields'];
            $mailService->save();
        }
    }
}

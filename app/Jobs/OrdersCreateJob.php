<?php

namespace App\Jobs;

use Config;
use App\Mail\OrderCreatedMail;
use App\Models\Shop;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class OrdersCreateJob implements ShouldQueue {

//class OrdersCreateJob {

    use Dispatchable,
        InteractsWithQueue,
        Queueable,
        SerializesModels;

    /**
     * Shop's myshopify domain
     *
     * @var string
     */
    public $shopDomain;

    /**
     * The webhook data
     *
     * @var object
     */
    public $data;

    /**
     * Create a new job instance.
     *
     * @param string $shopDomain The shop's myshopify domain
     * @param object $data    The webhook data (JSON decoded)
     *
     * @return void
     */
    public function __construct($shopDomain, $data) {
        $this->shopDomain = $shopDomain;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {

        $shopDomain = $this->shopDomain;
        $order = $this->data;
        $shop = Shop::where('shopify_domain', $shopDomain)->first();
        if ($shop->setting) {
            $config = $shop->setting->configuration;
            Config::set('mail.driver', $shop->setting->mailService->driver);
            Config::set('mail.host', $config['host']);
            Config::set('mail.port', $config['port']);
            Config::set('mail.from', ['address' => $config['from'], 'name' => 'Vendor Master App']);
            Config::set('mail.username', $config['username']);
            Config::set('mail.password', $config['password']);
            
            $vendors = array_column($order->line_items, 'vendor');
            $uniqueVendors = array_unique($vendors);

            foreach ($uniqueVendors as $vendor) {
                $keys = array_keys($vendors, $vendor);
                $lineItems = [];
                foreach ($keys as $key) {
                    $lineItems[] = $order->line_items[$key];
                }
                $mailOrder = [];
                $mailOrder['id'] = $order->id;
                $mailOrder['name'] = $order->name;
                $mailOrder['line_items'] = $lineItems;

                $vendorEmail = $shop->vendors()->where('name', $vendor)->pluck('email')->first();

                \Mail::to($vendorEmail)->send(new OrderCreatedMail($mailOrder));
            }
        }
    }

}

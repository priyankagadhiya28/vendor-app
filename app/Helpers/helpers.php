<?php

if (!function_exists('uuid')) {

    function uuid() {
        return (string) \Str::uuid();
    }

}

if (!function_exists('createOrDeleteFile')) {

    function createOrDeleteFile($path, $newFile, $existingFile = NULL) {
        $disk = "public";

        //delete existing file
        if ($existingFile) {
            deleteFile($path . '/' . $existingFile);
        }
        // if file is deleted
        if ($newFile == null) {
            return null;
        }
        
        \Log::info($newFile->getMimeType());
        //store new file
        $filename = md5($newFile . time()) . '.' . $newFile->getClientOriginalExtension();
        \Storage::disk($disk)->putFileAs($path, $newFile, $filename);
        return $filename;
    }

}

if (!function_exists('deleteFile')) {

    function deleteFile($filePath) {
        \Storage::disk('public')->delete($filePath);
    }

}
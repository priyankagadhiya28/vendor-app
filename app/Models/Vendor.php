<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model {
    /*
      |--------------------------------------------------------------------------
      | GLOBAL VARIABLES
      |--------------------------------------------------------------------------
     */

    public $incrementing = false;

//    protected $fillable = [''];

    /*
      |--------------------------------------------------------------------------
      | FUNCTIONS
      |--------------------------------------------------------------------------
     */
    public function deleteWithRelationalData() {
        $attributeName = "profile_picture";
        if (isset($this->attributes[$attributeName])) {
            $path = "vendors/profile-pictures";
            deleteFile($path . '/' . $this->attributes[$attributeName]);
        }
        $this->delete();
    }

    /*
      |--------------------------------------------------------------------------
      | RELATIONS
      |--------------------------------------------------------------------------
     */

    public function shop() {
        return $this->belongsTo(Shop::class);
    }

    /*
      |--------------------------------------------------------------------------
      | SCOPES
      |--------------------------------------------------------------------------
     */

    public function scopeFilter($query, $filters) {
        if (isset($filters['search'])) {
            $query->where('name', 'LIKE', '%' . $filters['search'] . '%');
            $query->orWhere('email', "LIKE", "%" . $filters['search'] . "%");
        }
    }

    /*
      |--------------------------------------------------------------------------
      | ACCESORS
      |--------------------------------------------------------------------------
     */

    public function getProfilePictureAttribute($value) {
        return ($value) ? url('/storage/vendors/profile-pictures/' . $value) : '';
    }

    /*
      |--------------------------------------------------------------------------
      | MUTATORS
      |--------------------------------------------------------------------------
     */

    public function setProfilePictureAttribute($value) {
        $attributeName = "profile_picture";
        $path = "vendors/profile-pictures";
        $existingFile = isset($this->attributes[$attributeName]) ? $this->attributes[$attributeName] : null;
        $this->attributes[$attributeName] = createOrDeleteFile($path, $value, $existingFile);
    }

}

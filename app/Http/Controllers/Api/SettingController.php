<?php

namespace App\Http\Controllers\Api;

use App\Models\MailService;
use App\Models\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingController extends Controller {

    public function get() {
        $shop = \ShopifyApp::shop();        
        $setting = $shop->setting;
        
        if(!$setting){
            $request = $shop->api()->rest('GET', '/admin/shop.json');
            $configuration = [];
            $configuration['from'] = $request->body->shop->email;
            
            $setting = new Setting;            
            $setting->configuration = $configuration;            
        }
        return $setting;
    }
    
    public function store(Request $request) {
        $shop = \ShopifyApp::shop();
        $shopSetting = $setting = $shop->setting;
        if(!$shopSetting){
            $setting = new Setting;            
            $setting->id = uuid();            
        }
        $setting->mail_service_id = $request->mail_service_id;
        $setting->configuration = $request->configuration;
        
        if(!$shopSetting){
            $shop->setting()->save($setting);
        }else{
            $setting->save();
        }
        return ['message' => "Mail settings has been updated successfully!."];
    }

}

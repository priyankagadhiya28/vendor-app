<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\VendorRequest;
use App\Models\Vendor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VendorController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return \ShopifyApp::shop()->vendors()->filter(request(['search']))->latest()->paginate(3);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VendorRequest $request) {
        $vendor = new Vendor;
        $vendor->id = uuid();
        $vendor->name = $request->name;
        $vendor->email = $request->email;
        $vendor->phone = $request->phone;
        $vendor->profile_picture = $request->profile_picture;
        $vendor->facebook_link = $request->facebook_link;
        $vendor->twitter_link = $request->twitter_link;
        $vendor->linkedin_link = $request->linkedin_link;
        \ShopifyApp::shop()->vendors()->save($vendor);

        return ['message' => "The $vendor->name vendor has been created successfully!."];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Vendor $vendor) {
        return $vendor;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(VendorRequest $request, Vendor $vendor) {
        $vendor->name = $request->name;
        $vendor->email = $request->email;
        $vendor->phone = $request->phone;
        if ($request->hasFile('profile_picture') || !$request->profile_picture) {
            $vendor->profile_picture = $request->profile_picture;
        }
        $vendor->facebook_link = $request->facebook_link;
        $vendor->twitter_link = $request->twitter_link;
        $vendor->linkedin_link = $request->linkedin_link;
        $vendor->save();

        return ['message' => "The $vendor->name vendor has been updated successfully!."];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vendor $vendor) {
        $vendor->deleteWithRelationalData();
        return ['message' => "The $vendor->name vendor has been deleted successfully!."];
    }

}
